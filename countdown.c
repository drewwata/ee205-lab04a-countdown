///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author TODO_name <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>

int main(int argc, char* argv[]) {

//Calling Present Time from time.h
   time_t now;
   time(&now);

//Variables for printing time
   int tm_year = 0 ;
   int tm_day = 0;
   int tm_hour = 0;
   int tm_min = 0;
   int tm_secs = 0;
//Varibles for time conversion
   int yr_in_secs = 31536000;
   int day_in_secs = 86400;
   int hour_in_secs = 3600;
   int min_in_secs = 60;


   int count;

//Declare and Define struct tm (Define Reference Time)
 struct tm start = {

      .tm_year = 118,
      .tm_mon = 9,
      .tm_mday = 22,
      .tm_hour = 10,
      .tm_min = 54,
      .tm_sec = 59
  }; 


//Convert start to string value
mktime(&start);

//Print struct tm (Reference Time) 
printf("The Reference time is: %s", asctime(&start));

//Compute Time Difference Between 'now' and 'start'
double total_seconds = difftime(now, mktime(&start));

//Print time differences (in seconds)
printf("%.f seconds have passed since Reference Date.\n", total_seconds);

//convert total 'seconds' into values of years, days, hours, minutes, and seconds 
   while(total_seconds >= yr_in_secs){
      total_seconds = total_seconds - yr_in_secs;
      tm_year = tm_year + 1;
   }

   while(total_seconds >= day_in_secs){

      total_seconds = total_seconds - day_in_secs;
      tm_day = tm_day + 1;
   }

   while(total_seconds >= hour_in_secs){

      total_seconds = total_seconds - hour_in_secs;
      tm_hour = tm_hour + 1;
   }

   while(total_seconds >= min_in_secs){

      total_seconds = total_seconds - min_in_secs;
      tm_min = tm_min + 1;
   }

tm_secs = total_seconds;

//Print Countdown Timer Values for 60 seconds
for(count = 0; count <= 60; count++ ){

//Delay by 1 second   
   sleep(1);

printf("Years: %d  Days: %d   Hours: %d   Minutes: %d   Seconds: %d\n", tm_year, tm_day, tm_hour, tm_min, tm_secs);

   tm_secs = tm_secs + 1;

      if(tm_secs == 60){
         tm_min = tm_min + 1;

         tm_secs = 0;
   }

      if(tm_min == 60){
         tm_hour = tm_hour + 1;

         tm_min = 0;
   }  

      if(tm_hour == 24){
         tm_day = tm_day + 1;

         tm_hour = 0;
   }  

      if(tm_day == 365){
         tm_year = tm_year + 1;

      tm_day = 0;
   }


}

   return 0;
}

